const Note = require('../models/note.model.js'); //requerimos el modelo en note.model.js

//Crear y guardar la nota
exports.create = (req, res) => {
    // Verificar que no este vacio
    if (!req.body.content) {
        return res.status(400).send({
            message: "La nota no puede estar vacia"
        });
    }

    // Crear la nota
    const note = new Note({
        title: req.body.title || "Nota sin titulo",
        content: req.body.content
    });

    // Guardar nota en la base de datos
    note.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Ocurrió un error al guardar la nota"
            });
        });
};



// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Note.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Ocurrió un error al recuperar las notas"
        });
    });
};

// Find a single note with a noteId
exports.findOne = (req, res) => {
    Note.findById(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Nota no encontrada con el ID " + req.params.noteId
            });            
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Nota no encontrada con el ID " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error recuperando la nota con el ID " + req.params.noteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
// Validate Request
if(!req.body.content) {
    return res.status(400).send({
        message: "La nota no puede estar vacia"
    });
}

// Find note and update it with the request body
Note.findByIdAndUpdate(req.params.noteId, {
    title: req.body.title || "Nota sin Titulo",
    content: req.body.content
}, {new: true})
.then(note => {
    if(!note) {
        return res.status(404).send({
            message: "Nota no encontrada con el ID " + req.params.noteId
        });
    }
    res.send(note);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "Nota no encontrada con el ID " + req.params.noteId
        });                
    }
    return res.status(500).send({
        message: "Error actualizando la nota con el ID " + req.params.noteId
    });
});
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Nota no encontrada con el ID " + req.params.noteId
            });
        }
        res.send({message: "Nota eliminada satisfactoriamente!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Nota no encontrada con el ID " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "No se pudo eliminar la nota con el ID " + req.params.noteId
        });
    });
};