const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    title: String,
    content: String
}, {
    timestamps: true
}); //modelo de la nota

module.exports = mongoose.model('Note', NoteSchema); //permitimos requerir el modelo de la nota para poder usarlo