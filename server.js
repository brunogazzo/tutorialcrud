const express = require('express');
const bodyParser = require('body-parser');
// configuramos la base de datos
const mongoose = require('mongoose');
const dbConfig = require('./config/database.config.js');

mongoose.Promise = global.Promise;

//creamos la app con express
const app = express();

//pedimos un parse del tipo de contenido de application/x-www-form-url encoded
app.use(bodyParser.urlencoded({ extended: true }))

//pedimos un parse del tipo de contenido de application/json
app.use(bodyParser.json())


// conectamos a la base de datos
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Conectado exitosamente a la base de datos");
}).catch(err => {
    console.log('No se pudo conectar a la base de datos, saliendo...', err);
    process.exit();
});

//definimos la ruta
app.get('/', (req, res) => {
    res.json({ 'Mensaje': "Bienvenido a la APP" });
});

// Require Notes routes
require('./app/routes/note.routes.js')(app);

//escuchamos los pedidos
app.listen(3000, () => {
    console.log("Escuchando en el puerto 3000");
});